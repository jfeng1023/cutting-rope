
from math import floor, ceil, sqrt
import numpy as np

'''
given a rope of the length "total", return the number of ways to cut it so that the longest segment is of the length "longest"
'''
def number_of_ways(total, longest):
    if total < longest+2 or total > longest*3:
        return 0
    if total == longest*3:
        return 1
    else:
        min_shortest = max(1, total-longest*2)
        max_shortest = floor((total-longest)/2)
        
        # first assuming all segments have different lengths, there are 6 permutations for each length combination
        count = (max_shortest - min_shortest + 1)*6
        # if the two shorter segments or two longer segments have the same length, there are only 3 permutations, so we subtract 3 from the count
        if max_shortest * 2 == total-longest:
            count -= 3
        if min_shortest == total-longest*2:
            count -= 3
    return count

'''
given a rope of the length L and the number of iteration n, return a dictionary that indicates the number of ways to get each possible final length
'''
def cut_rope(L, n):

    m = np.zeros((L+1,L+1))
    for i in range(3,L+1):
        for j in range(int(ceil(i/3)), i-1):
            m[i][j] = number_of_ways(i,j)

    current = {L:1}

    for i in range(n):
        last = current
        current = {}

        for length in last:
            if length < 3:  # it can't be cut further if the length isn't at least 3
                current[length] = last[length]
            else:
                max_longest = length - 2
                min_longest = ceil(length/3)
                for j in range(int(min_longest), int(max_longest+1)):
                    if j in current:
                        current[j] += m[length][j]*last[length]
                    else:
                        current[j] = m[length][j]*last[length]

    return current

def get_mean(result):
    total_length = sum([x*result[x] for x in result])
    total_number = sum([result[x] for x in result])
    return format(total_length/total_number,'.10g')

def get_stdev(result):
    total_length = sum([x*result[x] for x in result])
    total_number = sum([result[x] for x in result])
    mean = total_length/total_number
    sum_sq = sum([((x - mean)**2)*result[x] for x in result])
    stdev = sqrt(sum_sq/total_number)
    return format(stdev,'.10g')

def conditional_prob(result,high,low):
    high_count = sum([result[x] for x in result if x > high])
    low_count = sum([result[x] for x in result if x > low])
    return format(high_count/low_count,'.10g')

result_1 = cut_rope(64,5)
print(get_mean(result_1))
print(get_stdev(result_1))
print(conditional_prob(result_1,8,4))

result_2 = cut_rope(1024,10)
print(get_mean(result_2))
print(get_stdev(result_2))
print(conditional_prob(result_2,12,6))
