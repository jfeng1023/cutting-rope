# cutting-rope

This was a coding problem as part of the Data Incubator challenge in July 2018.

There is a rope of length N with points at interior integer coordinates (1,…,N−1). At every turn, we select 2 unique interior integer coordinates uniformly at random without replacement, cut the rope at those 2 coordinates, and take the longest of the resulting 3 ropes as our new rope. We do this iteratively T times (as long as it is possible to cut the rope at 2 unique interior coordinates) and call the length of the final resulting rope S. 

For two cases: 1) N=64, T=5; 2) N=1024, T=10, find out:
- the mean of S
- the standard deviation of S
- the conditional probability that S > 8 given that S > 4 (case 1) or the conditional probability that S > 12 given that S > 6 (case 2)
